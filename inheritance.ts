class Animals{
	name:string
	colour:string
	constructor(animalname,animalcolour){
	this.name=animalname
	this.colour=animalcolour
	}
	property(dist){
	console.log("%s move %d meters",this.name,dist)
	console.log("%s has colour %s",this.name,this.colour)
	}
}

var a1=new Animals('tomy','red')
a1.property(10)

class Aquatic extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    swim(distanceInMeters = 5) {
        console.log("%s swims..",this.name);
        super.property(distanceInMeters);
    }
    
}

class Terrestrial extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    walk(distanceInMeters = 45) {
        console.log("%s walks..",this.name);
        super.property(distanceInMeters);
    }
    
}

class Amphibians extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    swim_walk(distanceInMeters = 45) {
        console.log("Galloping...");
        //super.move(distanceInMeters);
        super.property(distanceInMeters);
    }
}

var fish = new Aquatic("Fish_Dolphin",'black');
var rabbit = new Terrestrial("Rabbit",'white');

fish.swim();
rabbit.walk(34);
