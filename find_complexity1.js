"use strict";
/*this program just return the complexity according to loops or nested loop
havent checked the condition in the loops*/
exports.__esModule = true;
var fs1 = require("fs");
var file = fs1.readFileSync('patern2.ts', 'utf8');
var file_data = file.split("\n");
//break it at "\n" and stroe it in string[]
//fuction return_loop_infdex() that will return the index of all loops  in program in a list
function return_loop_infdex(file_data) {
    var index_loop = [];
    for (var i = 0; i <= file_data.length; i++) {
        if (file_data[i] != undefined) {
            if (file_data[i].indexOf('for') >= 0 || file_data[i].indexOf('while') >= 0) {
                index_loop.push(i);
                //console.log(file_data[i],i)
            }
        }
    }
    return index_loop;
}
/*function return_bracket_loop() that returns closing bracket after for loop
so that we can detect the whether the exiting loops are nested or not*/
function return_bracket_loop(file_data, index_loop) {
    var bracket = [];
    var count = 1;
    for (var i = 0; i <= index_loop.length; i++) {
        for (var j = index_loop[i - 1]; j <= index_loop[i]; j++) {
            //console.log(i,j);
            if (file_data[j].indexOf('}') >= 0) {
                bracket.push(j);
                //console.log(file_data[j],j)
            }
        }
    }
    return bracket;
}
//function count_complexity() count complexity with the help of above data
function count_complexity(index_loop, bracket) {
    if (index_loop.length == 0) {
        return -1;
    }
    var comp = index_loop.length - bracket.length;
    if (comp <= 0) {
        return 1;
    }
    return comp;
}
//function print_complexity() print complexity
function print_complexity(comp) {
    var str1 = 'x';
    var str2 = '';
    if (comp == -1) {
        console.log("complexity of given program:" + 'O' + '(' + '1' + ')');
    }
    else {
        for (var i = 1; i <= comp; i++) {
            str1 = str1 + i;
            str2 = str2 + '*' + str1;
            str1 = 'x';
        }
        console.log("complexity of given program:" + 'O' + '(' + str2.slice(1) + ')');
    }
}
var index_loop = return_loop_infdex(file_data);
console.log(index_loop);
var bracket = return_bracket_loop(file_data, index_loop);
console.log(bracket);
var complexity = count_complexity(index_loop, bracket);
console.log(complexity);
print_complexity(complexity);
